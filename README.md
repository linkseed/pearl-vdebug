# vdebug for Pearl

Multi-language DBGP debugger client for Vim (PHP, Python, Perl, Ruby, etc.)

## Details

- Plugin: https://github.com/vim-vdebug/vdebug
- Pearl: https://github.com/pearl-core/pearl
